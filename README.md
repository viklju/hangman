# Basic Hangman Game
Written in three different versions
 - the original "hangman.py" Python version
 - the shortend Python version; "hangman-codegolf.py"
 - and the PowerShell version; "hangman.ps1"

## Motivation
This was a challenge to see who could first complete a simple hangman game, and then simplify it to least numnber of lines.
