word = 'random'; attempts = []; hangman = ['_'] * len(word)
while len(attempts) < 8:
    print(hangman, '\n', 'Guesses: ', attempts); guess = input('Guess: ').lower()
    if guess in word: hangman = [guess if guess == word[i] else '_' for i in range(len(word))]
    else: attempts.append(guess)
    if '_' not in hangman: print('You win!'); break
else: print('You lost, better luck next time')
