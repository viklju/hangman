$Word = "test"

# -----------------------------------------------------------------------------

$Word = $Word.ToLower()
$Hangman = '_ ' * $Word.Length
$Attempts = @()
$Tries = 0
$Loss = $True

While ($Tries -ne 8) {
  Write-Output $Hangman, ('-' * 30), (($Attempts | Sort-Object -Unique) -join ', '), ''
  If ($Hangman -notmatch '_') { Write-Output 'You win!'; $Loss = $False; break }
  $Guess = (Read-Host -Prompt 'Guess a letter: ').ToLower()

  If ($Guess.Length -ne 1 -or $Guess -notmatch '[a-z]') {
    Write-Output 'Error, invalid input', ''
  }
  Else {
    $Hit = 0
    ForEach ($Index in (0..$Word.Length)) {
      If ($Guess -eq $Word[$Index]) {
        $Hit = 1
        $Hangman = $Hangman.SubString(0, ($Index * 2)) + $Guess + $Hangman.SubString((($Index * 2) + 1))
        # extremly ugly and hard to understand
        # tried to do this instead: $Hangman[($Index * 2)] = $Guess
        # but that doesn't work for strings...
      }
    }
    If ($Hit -eq 0) {
      $Tries += 1
      Write-Output "Letter not in word, $Tries/8 wrong guesses", ''
      $Attempts += $Guess
    }
  }
}
If ($Loss -ne $False) { Write-Output 'Better luck next time!' }
Read-Host
