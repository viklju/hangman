word = "test"

# -----------------------------------------------------------------------------

word = word.lower()
hangman = ['_'] * len(word)
attempts = []
tries = 0

while tries != 8:
    misses = sorted(set(attempts))
    print(hangman, '-' * 30, misses, '\n', sep='\n')
    if '_' not in hangman:
        print('You win!')
        break

    guess = str(input('Guess a letter: ')).lower()

    if len(guess) != 1 or not guess.isalpha():
        print('Error, invalid input\n')
    elif guess in word:
        for index, letter in enumerate(word):
            if guess == letter:
                hangman[index] = letter
    elif guess not in word:
        tries += 1
        print('Letter not in word, ', tries, '/8 wrong guesses\n', sep='')
        attempts.append(guess)
else:
    print('Better luck next time!')
input('')
